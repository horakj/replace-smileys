
function hasClass(el, cls) {
	return el.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function addClass(el, cls) {
	if (!this.hasClass(el,cls)) el.className += " " + cls;
}

function removeClass(el,cls) {
	if (hasClass(el,cls)) {
    	var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
		el.className = el.className.replace(reg,' ');
	}
}

(function(d) {
    // write actual version
    var version = chrome.app.getDetails().version;

    if (version) {
        var el = d.getElementById('version');
        el.innerHTML = 'v' + version;
    }

    // copy codes on click
    codes = d.getElementsByTagName('code');

    for (i = 0, l = codes.length; i < l; i++) {
        code = codes[i];
        code.setAttribute('title', 'Copy to clipboad');
        code.setAttribute('style', 'cursor: pointer;');
        code.onclick = function() {
            removeClass(this, 'clicked');
            addClass(this, 'clicked');
            this.contentEditable = true;
            this.focus();
            document.execCommand('SelectAll');
            document.execCommand('copy');
            this.contentEditable = false;
            this.blur();
            removeClass(this, 'clicked');
        };
    }

})(document);
